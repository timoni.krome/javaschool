package main;

import basis.Farbe;
import basis.Fenster;
import basis.Hilfe;
import basis.IgelStift;
import basis.Maus;
import basis.MausKlick;
import basis.Stift;

import java.awt.Rectangle;

import javax.swing.JButton;
import javax.swing.JFrame;

public class ArbeitIgelStift {
	Returner returnedValues = startsequenz();
	IgelStift stift1 = returnedValues.getStift();
	Fenster fenster1 = returnedValues.getFenster();
	Maus maus1 = returnedValues.getMaus();
	
	double degree;
	double valueforDegree;
	double lenghOfSidesPi;


	
	public ArbeitIgelStift() {
		// TODO Auto-generated constructor stub
		fenster1.setzeGroesse(1920, 1080);
	}
	
	public void farbeRot() {
		stift1.setzeFarbe(Farbe.ROT);
	}
	
	public void farbeBlau() {
		stift1.setzeFarbe(Farbe.BLAU);
		
	}
	
	public void farbeGrün() {
		stift1.setzeFarbe(Farbe.GRUEN);
	}
	
	public void farbeCyan() {
		stift1.setzeFarbe(Farbe.CYAN);
	}
	
	public void farbeDunkelGrau() {
		stift1.setzeFarbe(Farbe.DUNKELGRAU);
	}
	
	public void farbeGelb() {
		stift1.setzeFarbe(Farbe.GELB);
	}
	
	public void farbeSchwarz() {
		stift1.setzeFarbe(Farbe.SCHWARZ);
	}
	
	public void farbeHellgrau() {
		stift1.setzeFarbe(Farbe.HELLGRAU);
	}
	
	public void farbeMagenta() {
		stift1.setzeFarbe(Farbe.MAGENTA);
	}
	
	public void farbeOrange() {
		stift1.setzeFarbe(Farbe.ORANGE);
	}
	
	public void farbePink() {
		stift1.setzeFarbe(Farbe.PINK);
	}
	
	public void farbeWeiß() {
		stift1.setzeFarbe(Farbe.WEISS);
	}
	
	public void fuehreAus() {
		/*Arbeiten, Aktionsteil */
		
		stift1.bewegeBis(maus1.hPosition(),maus1.vPosition());
		stift1.runter();
		while(! maus1.istGedrueckt()) {
		stift1.runter();
		Hilfe.kurzePause();
		Hilfe.kurzePause();
		Hilfe.kurzePause();
		stift1.bewegeBis(maus1.hPosition(),maus1.vPosition());
		System.out.println(maus1.hPosition() + " " + maus1.vPosition()+" "+stift1.hPosition()+" "+stift1.vPosition());
		}
		
		}
	
	public void drawWithMouse() {
		boolean läuft = true;
		while(läuft) {
				if (maus1.istGedrueckt()) {
					stift1.bewegeAuf(maus1.hPosition(), maus1.vPosition());
					stift1.runter();
				}
				MausKlick klick = maus1.holeKlick();
				if(klick.isLinks()) {
					läuft = false;
					System.out.print("stop");
				}
			
		}
	}
	public void squareSixty() {
		//Fenster fenster1 = new Fenster();
		
		
		/*
		stift1.bewegeBis(200, 150);
		stift1.dreheBis(0);
		stift1.runter();
		*/
		
		//Add Second Button
		for(int i = 0; i < 4; i++) {
			stift1.bewegeUm(60);
			stift1.dreheUm(90);
		}
	}
	public Returner startsequenz() {
		//Create Window and brush
		Fenster fenster1 = new Fenster();
		IgelStift stift1 = new IgelStift();
		Maus maus1 = new Maus();
		
		//Create Return Object
		Returner Returner = new Returner(stift1, fenster1, maus1);
		
		stift1.bewegeBis(360, 340);
		stift1.dreheBis(0);
		stift1.runter();
		return Returner;
	}
	public void taskOnepointOne() {
		//IgelStift stift1 = startsequenz();
		for(int i = 0; i < 3; i++) {
			stift1.bewegeUm(30);
			stift1.dreheUm(120);
		}
	}
	
	
	public void taskOnePointThree() {
		//IgelStift stift1 = startsequenz();
		double breite = 30;
		double länge = 60;
		double diagonal = (Math.sqrt(länge*länge+breite*breite));
		
		drawRectangle(stift1, länge, breite);
		stift1.dreheUm(90);
		stift1.bewegeUm(breite/2);
		stift1.dreheUm(-90);
		stift1.bewegeUm(länge);
		stift1.dreheUm(90);
		stift1.bewegeUm(breite/2);
		stift1.dreheUm(116.565);
		stift1.bewegeUm(diagonal);
		stift1.dreheUm(243.435);
		stift1.bewegeUm(breite);
		stift1.dreheUm(-116.565);
		stift1.bewegeUm(diagonal);
		
	}
	public static void drawRectangle(IgelStift stift1, double länge, double breite) {
		stift1.runter();
		for(int i = 0; i < 2; i++) {
			stift1.bewegeUm(länge);
			stift1.dreheUm(90);
			stift1.bewegeUm(breite);
			stift1.dreheUm(90);
			
		}
	}
	public void taskOnePointEleven() {
		// TODO Auto-generated method stub
		int Länge = 150;
		for(int i = 0; i < 4; i++) {
			stift1.bewegeUm(Länge);
			stift1.dreheUm(90);
		}
		stift1.dreheUm(90);
		stift1.bewegeUm(Länge);
		stift1.dreheUm(-90);
		for(int i = 0; i < 3; i++) {
			stift1.bewegeUm(Länge);
			stift1.dreheUm(360/3);
		}
		stift1.dreheUm(-45);
		stift1.bewegeUm(Math.sqrt(2*(Länge*Länge)));
		stift1.dreheUm(45+90);
		stift1.bewegeUm(Länge);
		stift1.dreheUm(45+90);
		stift1.bewegeUm(Math.sqrt(2*(Länge*Länge)));
		stift1.dreheUm(90+45);
		stift1.bewegeUm(Länge);
	}
	
	public void circleWithCross() {
		int radius = 80;
		
		stift1.bewegeUm(radius*2);
		stift1.dreheUm(180);
		stift1.bewegeUm(radius);
		stift1.dreheUm(90);
		stift1.bewegeUm(radius);
		stift1.dreheUm(180);
		stift1.bewegeUm(radius*2);
		stift1.dreheUm(90);
		double Umfang = 2*Math.PI*radius;
		double edges = 10000;
		double lenghPerSide;
		if(lenghOfSidesPi !=0) {
			lenghPerSide = lenghOfSidesPi;
		} else {
		lenghPerSide = Umfang/edges;
		lenghOfSidesPi = lenghPerSide;
		}
		
		drawPolygon(edges, lenghPerSide);
		stift1.dreheUm(90);
		stift1.bewegeUm(radius);
		stift1.dreheUm(-90);
		stift1.bewegeUm(radius);
		stift1.dreheUm(180);
		
	}
	
	public void drawTree() {
		int heightTree = 80;
		int heightStamm = 20;
		int breite = 50;
		int breiteStamm = breite/5;
		int breiteBaumEineSeite = breite-breiteStamm;
		double Xposition = stift1.holeZustand().getStiftx();
		double Yposition = stift1.holeZustand().getStifty();
		double corner1X = Xposition+breiteStamm;
		double corner1Y = Yposition;
		double corner2X = corner1X;
		double corner2Y = Yposition-heightStamm;
		double corner3X = Xposition+breiteBaumEineSeite;
		double corner3Y = corner2Y;
		double corner4X = Xposition + breiteStamm/2;
		double corner4Y = corner3Y - heightTree;
		double corner5X = Xposition-breiteBaumEineSeite+breiteStamm/2;
		double corner5Y = corner2Y;
		double corner6X = Xposition;
		double corner6Y = Yposition-heightStamm;
		
		stift1.bewegeAuf(corner1X, corner1Y);
		stift1.bewegeAuf(corner2X, corner2Y);
		stift1.bewegeAuf(corner3X, corner3Y);
		stift1.bewegeAuf(corner4X, corner4Y);
		stift1.bewegeAuf(corner5X, corner5Y);
		stift1.bewegeAuf(corner6X, corner6Y);
		stift1.bewegeAuf(Xposition, Yposition);
		stift1.dreheUm(90);
		
	}
	
	public void drawPolygon(double edges,double lenghsides) {
		for(int i = 0; i < edges; i++) {
			if(degree != 0 && valueforDegree == edges) {
				stift1.bewegeUm(lenghsides);
				stift1.dreheUm(degree);
			} else {
				stift1.bewegeUm(lenghsides);
				degree = 360/edges;
				stift1.dreheUm(degree);
				valueforDegree = edges;
			}
		}
	}
	
	public void drawOtherCircleWithSquare() {
		int radius = 80;
		stift1.hoch();
		stift1.bewegeUm(radius);
		stift1.dreheUm(45+180);
		stift1.bewegeUm(radius);
		stift1.dreheUm(180);
		stift1.runter();
		circleWithCross();
		stift1.hoch();
		stift1.bewegeUm(radius);
		stift1.dreheUm(180-45);
		stift1.bewegeUm(radius);
		stift1.dreheUm(180);
		stift1.runter();
	}
	
	public void drawSetzkasten() {
		
		//Draw SetzKasten Aus 6 Quadraten
		
		//Zeilen
		for(int i = 0; i<2;i++) {
			
			//Spalten
			for(int j = 0; j<3;j++) {
				drawPolygon(4, 200);
				stift1.bewegeUm(200);
			}
			stift1.dreheUm(180);
		}
		stift1.hoch();
		
		double Xposition1 = stift1.holeZustand().getStiftx();
		double Yposition1 = stift1.holeZustand().getStifty();
		double drehung1 = stift1.holeZustand().getWinkel();
		
		
		for(int i = 0; i < 2; i++) {
			
			
			//fülle Quadrate Mit inhalt
			
			
			
			//erstes und viertes Quadrat
			
			double Xposition = stift1.holeZustand().getStiftx();
			double Yposition = stift1.holeZustand().getStifty();
			double drehung = stift1.holeZustand().getWinkel();
			stift1.hoch();
			stift1.dreheUm(90);
			stift1.bewegeUm(100);
			stift1.dreheUm(-90);
			stift1.bewegeUm(20);
			drawOtherCircleWithSquare();
			stift1.hoch();
			stift1.bewegeAuf(Xposition, Yposition);
			stift1.dreheBis(drehung);
			
			//zweiter und fünftes Quadrat
			stift1.bewegeUm(220);
			Xposition = stift1.holeZustand().getStiftx();
			Yposition = stift1.holeZustand().getStifty();
			drehung = stift1.holeZustand().getWinkel();

			
			
			stift1.dreheUm(90);
			stift1.bewegeUm(100);
			stift1.dreheUm(-90);
			stift1.runter();
			circleWithCross();
			stift1.hoch();
			stift1.bewegeAuf(Xposition, Yposition);
			stift1.dreheBis(drehung);
			stift1.bewegeUm(180);
			
			//drittes Quadrat
			
			if(i == 0) {
			
			Xposition = stift1.holeZustand().getStiftx();
			Yposition = stift1.holeZustand().getStifty();
			drehung = stift1.holeZustand().getWinkel();
			
			
			stift1.bewegeUm(80);
			stift1.dreheUm(90);
			stift1.bewegeUm(20);
			stift1.dreheUm(-90);
			
			stift1.runter();
			drawTree();
			stift1.hoch();
			
			stift1.bewegeAuf(Xposition+200, Yposition);
			stift1.dreheBis(drehung+180);
			}
		}
		
		stift1.dreheBis(drehung1);
		stift1.bewegeAuf(Xposition1+80, Yposition1+180);
		stift1.dreheBis(drehung1);
		stift1.runter();
		drawTree();
		
		
		
		
	}
	
	public void pinselGroeßer() {
		stift1.setzeLinienBreite(stift1.linienBreite()*2);
	}
	
	public void pinselKleiner() {
		stift1.setzeLinienBreite(stift1.linienBreite()/2);
	}
	
	public void pinselStandard() {
		stift1.setzeLinienBreite(1);
	}
	

}
