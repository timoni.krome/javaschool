package main;

import basis.Fenster;
import basis.IgelStift;
import basis.Maus;

public class Returner {
	
	private IgelStift stift;
	private Fenster fenster;
	private Maus maus;

	public Returner(IgelStift stift1, Fenster fenster1, Maus maus1) {
		// TODO Auto-generated constructor stub
		stift = stift1;
		fenster = fenster1;
		maus = maus1;
		
	}
	public IgelStift getStift() {
		return stift;
	}
	public Fenster getFenster() {
		return fenster;
	}
	
	public Maus getMaus() {
		return maus;
	}

}
