package main;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import basis.IgelStift;

public class GUI implements ActionListener{

	
	//Create Variables etc
	int count = 0;
	private JPanel panel;
	private JFrame frame;
	private JButton button;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	private JButton Farbe[] = new JButton[12];
	private JButton Kontrolle[] = new JButton[9];
	
	
	
	//private JButton button5;
	ArbeitIgelStift Fenster = new ArbeitIgelStift();
	//IgelStift stift1 = Fenster.startsequenz();
	
	//Constructor
	public GUI() {
		frame = new JFrame();
		
		//Add First button
		button = new JButton("Draw Square");
		button.addActionListener(this);
		
		//Add Second button
		button2 = new JButton("Task 1.1");
		button2.addActionListener(this);
		
		//Add Third button
		button3 = new JButton("Task 1.3");
		button3.addActionListener(this);
		
		//Add Fourth button
		button4 = new JButton("Task 1.11");
		button4.addActionListener(this);
		
		//Add Fith button
		button5= new JButton("Task 1.13");
		button5.addActionListener(this);
		
		//Add six button
		button6 = new JButton("Use Mous (Task 2.3)");
		button6.addActionListener(this);
		
		//Add Farbe Buttons
		Farbe[0] = new JButton("Farbe: Blau");
		Farbe[1] = new JButton("Farbe: Cyan");
		Farbe[2] = new JButton("Farbe: Dunkelgrau");
		Farbe[3] = new JButton("Farbe: Gelb");
		Farbe[4] = new JButton("Farbe: Grün");
		Farbe[5] = new JButton("Farbe: Hellgrau");
		Farbe[6] = new JButton("Farbe: Magenta");
		Farbe[7] = new JButton("Farbe: Orange");
		Farbe[8] = new JButton("Farbe: Pink");
		Farbe[9] = new JButton("Farbe: Rot");
		Farbe[10] = new JButton("Farbe: Schwarz");
		Farbe[11] = new JButton("Radiergummi");
		
		int ii = 0;
		while(ii < 12) {
		Farbe[ii].addActionListener(this);
		ii++;
		}
		
		//Add Control Buttons
		Kontrolle[0] = new JButton("Links Drehen (22.5°)");
		Kontrolle[1] = new JButton("Vorwärts Bewegen(10PX)");
		Kontrolle[2] = new JButton("Rechts Drehen(22.5°)");
		Kontrolle[3] = new JButton("Vorwärts Bewgen(1PX)");
		Kontrolle[4] = new JButton("Pinsel Hoch");
		Kontrolle[5] = new JButton("Pinsel Runter");
		Kontrolle[6] = new JButton("Pinselbreite -");
		Kontrolle[7] = new JButton("Pinselbreite Standard");
		Kontrolle[8] = new JButton("Pinselbreite +");
		
		for(int i = 0; i < Kontrolle.length; i++) {
			Kontrolle[i].addActionListener(this);
		}
		
		
		
		
		panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 10, 10));
		GridLayout grid = new GridLayout();
		//grid.setColumns(6);
		grid.setRows(7);
		panel.setLayout(grid);
		panel.add(button);
		panel.add(button2);
		panel.add(button3);
		panel.add(button4);
		panel.add(button5);
		//panel.add(button6);
		for(int i = 0; i < 12; i++) {
			panel.add(Farbe[i]);
		}
		for(int i = 0; i < Kontrolle.length; i++) {
			panel.add(Kontrolle[i]);
		}
		
		frame.add(panel, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("My Application");
		frame.pack();
		frame.setVisible(true);
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		//Add Action to the Buttons
		if(e.getSource() == button){
			Fenster.squareSixty();
			
		}
		if(e.getSource() == button2){
			Fenster.taskOnepointOne();
			
		} if(e.getSource() == button3) {
			Fenster.taskOnePointThree();
		} if(e.getSource() == button4) {
			Fenster.taskOnePointEleven();
		} if(e.getSource() == button5) {
			Fenster.drawSetzkasten();
			
		} if(e.getSource() == button6) {
			
			Fenster.fuehreAus();
			
		} if(e.getSource() == Farbe[0]) {
			Fenster.farbeBlau();
		} if(e.getSource() == Farbe[1]) {
			Fenster.farbeCyan();
		} if(e.getSource() == Farbe[2]) {
			Fenster.farbeDunkelGrau();
		} if(e.getSource() == Farbe[3]) {
			Fenster.farbeGelb();
		} if(e.getSource() == Farbe[4]) {
			Fenster.farbeGrün();
		} if(e.getSource() == Farbe[5]) {
			Fenster.farbeHellgrau();
		} if(e.getSource() == Farbe[6]) {
			Fenster.farbeMagenta();
		} if(e.getSource() == Farbe[7]) {
			Fenster.farbeOrange();
		} if(e.getSource() == Farbe[8]) {
			Fenster.farbePink();
		} if(e.getSource() == Farbe[9]) {
			Fenster.farbeRot();
		} if(e.getSource() == Farbe[10]) {
			Fenster.farbeSchwarz();
		} if(e.getSource() == Farbe[11]) {
			Fenster.farbeWeiß();
		} if(e.getSource() == Kontrolle[0]) {
			Fenster.stift1.dreheUm(22.5);
		} if(e.getSource() == Kontrolle[1]) {
			Fenster.stift1.bewegeUm(10);
		} if(e.getSource() == Kontrolle[2]) {
			Fenster.stift1.dreheUm(-22.5);
		} if(e.getSource() == Kontrolle[3]) {
			Fenster.stift1.bewegeUm(1);
		} if(e.getSource() == Kontrolle[4]) {
			Fenster.stift1.hoch();
		} if(e.getSource() == Kontrolle[5]) {
			Fenster.stift1.runter();
		} if(e.getSource() == Kontrolle[6]) {
			Fenster.pinselKleiner();
		} if(e.getSource() == Kontrolle[7]) {
			Fenster.pinselStandard();
		} if(e.getSource() == Kontrolle[8]) {
			Fenster.pinselGroeßer();
		}
	}
}