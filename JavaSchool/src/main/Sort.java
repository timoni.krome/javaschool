package main;

import java.util.Random;

public class Sort {

	public Sort() {
		// TODO Auto-generated constructor stub
	}
	
	public static int[] würfel() {
		int[] wuerfelergebnisse = new int[6];
		for(int i = 0; i <6; i++) {
			wuerfelergebnisse[i] = i+1;
		}
		return wuerfelergebnisse;
	}
	
	public static int getRandomNumberUsingNextInt(int min, int max) {
	    Random random = new Random();
	    return random.nextInt(max - min) + min;
	}
	
	public static int[] würfelSimulation() {
		//create Array
		int[] ergebnisse = new int[30];
		
		//Run 30 times
		for(int i = 0; i < 30; i++) {
			
			//get Random value for dice
			int randomValue = getRandomNumberUsingNextInt(1, 6);
			ergebnisse[i] = randomValue;
		}
		return ergebnisse;
	}

}
